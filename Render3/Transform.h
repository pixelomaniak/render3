#pragma once
#include <vector>
#include <memory>

#include <boost/noncopyable.hpp>
#include <glm/glm.hpp>

namespace Render3
{
	class Transform 
	{
	public:
		static const glm::vec3 right;
		static const glm::vec3 up;
		static const glm::vec3 forward;
		static const glm::mat4x4 identity;

		Transform();
		Transform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale);
		virtual ~Transform();

	public:
		/* Transform settings */
		glm::mat4x4 getMatrix();
		glm::mat4x4 getLocalMatrix();

		glm::vec3 position;
		glm::vec3 rotation;
		glm::vec3 scale;

		glm::vec3 localPosition;
		glm::vec3 localRotation;
		glm::vec3 localScale;

	public:
		/* Heirarchy settings */
		void setParent(const std::shared_ptr<Transform>& transform);
		void addChild(const std::shared_ptr<Transform>& transform);
		void removeChild(const std::shared_ptr<Transform>& transform);

	protected:
		std::weak_ptr<Transform> mParent;
		std::vector<std::shared_ptr<Transform>> mChildren;
	};

	using TransformSharedPtr = std::shared_ptr<Transform>;
	using TransformWeakPtr = std::weak_ptr<Transform>;
}
