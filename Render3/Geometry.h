#pragma once

#include <memory>
#include <vector>

#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>
#include <glm/glm.hpp>

namespace Render3
{
	struct Vertex
	{
		Vertex(const glm::vec3& position);
		Vertex(const glm::vec3& position,
			   const boost::optional<glm::vec2>& texCoords,
			   const boost::optional<glm::vec3>& normal);

		glm::vec3 mPosition;
		boost::optional<glm::vec2> mTexCoord;
		boost::optional<glm::vec3> mNormal;
	};

	class Geometry : private boost::noncopyable
	{
	public:
		friend class GeometryBuilder;
		virtual ~Geometry();

		const std::vector<Vertex>& getVertices() const;
		const std::vector<unsigned int>& getIndices() const;
		const std::vector<glm::vec3> getPositions() const;
		const std::vector<glm::vec2> getUvCoords() const;
		const std::vector<glm::vec3> getNormals() const;

	private:
		void Geometry::addVertex(const Vertex& vertex);
		void Geometry::addVertex(const glm::vec3 & position,
			const boost::optional<glm::vec2>& texCoords,
			const boost::optional<glm::vec3>& normal);
		void Geometry::addIndex(const unsigned int index);
		void Geometry::resetVertices(const std::vector<Vertex>& vertices);
		void Geometry::resetIndices(const std::vector<unsigned int>& indices);

	private:
		std::vector<Vertex> mVertices;
		std::vector<unsigned int> mIndices;
	};

	using GeometrySharedPtr = std::shared_ptr<Geometry>;

	class GeometryBuilder : private boost::noncopyable
	{
	public:
		static GeometrySharedPtr buildQuad();
		static GeometrySharedPtr buildBox();
		static GeometrySharedPtr buildSphere(unsigned int horizontalSegments = 8u,
											 unsigned int verticalSegments = 16u);

	};
}
