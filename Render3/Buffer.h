#pragma once

#include <string>
#include <memory>

#include <boost/noncopyable.hpp>
#include <GL/glew.h>

namespace Render3
{
	enum class BufferBindingTarget
	{
		// TODO: Cover all as specified @http://docs.gl/gl4/glBindBuffer
		GlArrayBuffer = GL_ARRAY_BUFFER,
		GlElementArrayBuffer = GL_ELEMENT_ARRAY_BUFFER,
		GlPixelPackBuffer = GL_PIXEL_PACK_BUFFER,
		GlPixelUnpackBuffer = GL_PIXEL_UNPACK_BUFFER,
		GlUniformBuffer = GL_UNIFORM_BUFFER,
		GlTextureBuffer = GL_TEXTURE_BUFFER,
		GlTransformFeedbackBuffer = GL_TRANSFORM_FEEDBACK_BUFFER,
		GlCopyReadBuffer = GL_COPY_READ_BUFFER,
		GlCopyWriteBuffer = GL_COPY_WRITE_BUFFER
	};

	enum class BufferUsage
	{
		GlStreamDraw = GL_STREAM_DRAW,
		GlStreamRead = GL_STREAM_READ,
		GlStreamCopy = GL_STREAM_COPY,
		GlStaticDraw = GL_STATIC_DRAW,
		GlStaticRead = GL_STATIC_READ,
		GlStaticCopy = GL_STATIC_COPY,
		GlDynamicDraw = GL_DYNAMIC_DRAW,
		GlDynamicRead = GL_DYNAMIC_READ,
		GlDynamicCopy = GL_DYNAMIC_COPY
	};

	class Buffer : private boost::noncopyable
	{
	public:
		Buffer(const BufferBindingTarget target, const BufferUsage usage, const unsigned int totalSize);

		~Buffer();

		void bufferSubData(const unsigned int offset,
						   const unsigned int subDataSize,
						   const void* const data);
		unsigned int getSize() const;

		void bind();
		void release();

	private:
		const GLuint getBufferId() const;

	private:
		GLuint mBuffer;

		GLenum mTarget;
		GLenum mUsage;
		unsigned int mSize;
		std::string mLabel;
	};

	class Vao : private boost::noncopyable
	{
	public:
		Vao();
		virtual ~Vao();

		void bind();
		void release();

	private:
		GLuint mVaoId;
	};

	using VaoSharedPtr = std::shared_ptr<Vao>;
	using BufferSharedPtr = std::shared_ptr<Buffer>;
}

