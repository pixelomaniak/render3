#include "RenderWindow.h"

#include <iostream>

#include <GL/glew.h>
#include <SDL.h>

#include "Common.h"

RenderWindow::RenderWindow(int width, int height, const std::string& title)
{
	SDL_Init(SDL_INIT_VIDEO);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

	mWindow = std::unique_ptr<SDL_Window, std::function<void(SDL_Window*)>>(SDL_CreateWindow(title.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
		height,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN),
		[this](SDL_Window* window)
		{
			SDL_GL_DeleteContext(mGlContext);
			SDL_DestroyWindow(window);
		});

	// TODO: Remove "exit" code.
	mGlContext = SDL_GL_CreateContext(mWindow.get());

	if (mGlContext == NULL)
	{
		std::cerr << "Faild to create OpenGL context" << std::endl;
		exit(EXIT_FAILURE);
	}

	// https://stackoverflow.com/questions/13558073/program-crash-on-glgenvertexarrays-call
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cerr << "Failed to initialize GLEW" << std::endl;
		exit(EXIT_FAILURE);
	}
	glClearColor(1.0, 0.0, 0.0, 1.0);
	SDL_GL_SetSwapInterval(1);
	this->mIsClosed = false;

	Render3::GlError::checkGlState();
}

void RenderWindow::update()
{
	SDL_Event e;
	if (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT) {
			mIsClosed = true;
		}
	}
	SDL_GL_SwapWindow(mWindow.get());
}

bool RenderWindow::isClosed()
{
	return mIsClosed;
}

RenderWindow::~RenderWindow()
{
	SDL_Quit();
}
