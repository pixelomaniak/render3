#include "Geometry.h"

namespace Render3
{
	Vertex::Vertex(const glm::vec3& position)
		: mPosition(position)
	{}

	Vertex::Vertex(const glm::vec3& position,
				   const boost::optional<glm::vec2>& texCoords,
				   const boost::optional<glm::vec3>& normal)

		: mPosition(position),
		  mTexCoord(texCoords),
		  mNormal(normal)
	{}

	Geometry::~Geometry() {}

	void Geometry::addVertex(const Vertex& vertex)
	{
		mVertices.push_back(vertex);
	}

	void Geometry::addVertex(const glm::vec3& position,
							 const boost::optional<glm::vec2>& texCoords,
							 const boost::optional<glm::vec3>& normal)
	{
		Vertex vertex(position, texCoords, normal);
		mVertices.push_back(vertex);
	}

	void Geometry::addIndex(const unsigned int index)
	{
		mIndices.push_back(index);
	}

	void Geometry::resetVertices(const std::vector<Vertex>& vertices)
	{
		mVertices = vertices;
	}

	void Geometry::resetIndices(const std::vector<unsigned int>& indices)
	{
		mIndices = indices;
	}

	const std::vector<Vertex>& Geometry::getVertices() const
	{
		return mVertices;
	}

	const std::vector<unsigned int>& Geometry::getIndices() const
	{
		return mIndices;
	}

	const std::vector<glm::vec3> Geometry::getPositions() const
	{
		std::vector<glm::vec3> result;
		result.reserve(mVertices.size());
		for (auto& vertex : mVertices)
		{
			result.push_back(vertex.mPosition);
		}
		return result;
	}

	const std::vector<glm::vec2> Geometry::getUvCoords() const
	{
		std::vector<glm::vec2> result;
		result.reserve(mVertices.size());
		for (auto& vertex : mVertices)
		{
			if (vertex.mTexCoord)
			{
				result.push_back(*vertex.mTexCoord);
			}
		}
		if (result.size() == 0)
		{
			result.shrink_to_fit();
		}
		return result;
	}

	const std::vector<glm::vec3> Geometry::getNormals() const
	{
		std::vector<glm::vec3> result;
		result.reserve(mVertices.size());
		for (auto& vertex : mVertices)
		{
			if (vertex.mNormal)
			{
				result.push_back(*vertex.mNormal);
			}
		}
		if (result.size() == 0)
		{
			result.shrink_to_fit();
		}
		return result;
	}

	GeometrySharedPtr GeometryBuilder::buildQuad()
	{
		GeometrySharedPtr result = std::make_shared<Geometry>();
		
		glm::vec3 normal = glm::vec3(0.0f, 0.0f, 1.0f);
		result->addVertex(glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec2(0.0f, 0.0f), normal);
		result->addVertex(glm::vec3(.7f, -.7f, 0.0f), glm::vec2(1.0f, 0.0f), normal);
		result->addVertex(glm::vec3(1.0f, 1.0f, 0.0f), glm::vec2(1.0f, 1.0f), normal);
		result->addVertex(glm::vec3(-.7f, .7f, 0.0f), glm::vec2(0.0f, 1.0f), normal);
		result->resetIndices({0, 1, 2, 0, 2, 3});

		return result;
	}

	GeometrySharedPtr GeometryBuilder::buildBox()
	{
		GeometrySharedPtr result = std::make_shared<Geometry>();

		// TODO: Add normals
		result->addVertex(glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec2(1.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(-1.0f, 1.0f, -1.0f), glm::vec2(0.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, 1.0f, -1.0f), glm::vec2(0.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, -1.0f, -1.0f), glm::vec2(1.0f, 1.0f), boost::none);

		result->addVertex(glm::vec3(-1.0f, -1.0f, 1.0f), glm::vec2(1.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(-1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, -1.0f, 1.0f), glm::vec2(1.0f, 1.0f), boost::none);

		result->addVertex(glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec2(0.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(-1.0f, -1.0f, 1.0f), glm::vec2(1.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, -1.0f, 1.0f), glm::vec2(1.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, -1.0f, -1.0f), glm::vec2(0.0f, 0.0f), boost::none);

		result->addVertex(glm::vec3(-1.0f, 1.0f, -1.0f), glm::vec2(0.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(-1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, 1.0f, -1.0f), glm::vec2(0.0f, 0.0f), boost::none);

		result->addVertex(glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec2(1.0, 1.0), boost::none);
		result->addVertex(glm::vec3(-1.0f, -1.0f, 1.0), glm::vec2(1.0, 0.0), boost::none);
		result->addVertex(glm::vec3(-1.0f, 1.0f, 1.0), glm::vec2(0.0, 0.0), boost::none);
		result->addVertex(glm::vec3(-1.0f, 1.0f, -1.0), glm::vec2(0.0, 1.0), boost::none);

		result->addVertex(glm::vec3(1.0f, -1.0f, -1.0f), glm::vec2(1.0f, 1.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, -1.0f, 1.0f), glm::vec2(1.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 0.0f), boost::none);
		result->addVertex(glm::vec3(1.0f, 1.0f, -1.0f), glm::vec2(0.0f, 1.0f), boost::none);

		result->resetIndices({0, 1, 2,
							  0, 2, 3,
							  6, 5, 4,
							  7, 6, 4,
							  10, 9, 8,
							  11, 10, 8,
							  12, 13, 14,
							  12, 14, 15,
							  16, 17, 18,
							  16, 18, 19,
							  22, 21, 20,
							  23, 22, 20
							 });
		return result;
	}
}