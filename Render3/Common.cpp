#include "Common.h"

#include <iostream>
#include <fstream>

#include <boost/assert.hpp>
#include <boost/format.hpp>

#include "GL/glew.h"

namespace Render3
{
	GlErrorType GlError::getGlError()
	{
		return static_cast<GlErrorType>(glGetError());
	}

	bool GlError::checkGlState()
	{
		bool result = false;
		GlErrorType error = getGlError();
		result = (error == GlErrorType::GlNoError);
		while (error != GlErrorType::GlNoError)
		{
			std::cout << boost::format("OpenGL encountered an error with code %x") % static_cast<GLenum>(error)
				      << std::endl;

			error = getGlError();
		}
		return result;
	}

	GlState& GlState::getInstance()
	{
		static GlState instance;
		return instance;
	}

	const bool GlState::bindShaderProgram(const GLuint shaderProgram)
	{
		if (shaderProgram != mCurrentlyBoundShaderProgram)
		{
			mCurrentlyBoundShaderProgram = shaderProgram;
			glUseProgram(shaderProgram);
			return true;
		}
		return false;
	}

	const bool GlState::releaseShaderProgram(const GLuint shaderProgram)
	{
		if (shaderProgram == mCurrentlyBoundShaderProgram)
		{
			glUseProgram(0);
			mCurrentlyBoundShaderProgram = 0;
			return true;
		}
		return false;
	}

	const bool GlState::bindVao(const GLuint vao)
	{
		if (vao != mCurrentlyBoundVao)
		{
			mCurrentlyBoundVao = vao;
			glBindVertexArray(vao);
			return true;
		}
		return false;
	}

	const bool GlState::releaseVao(const GLuint vao)
	{
		if (vao == mCurrentlyBoundVao)
		{
			glBindVertexArray(0);
			mCurrentlyBoundVao = 0;
			return true;
		}
		return false;
	}

	namespace Utils
	{
		std::string readAllFile(const std::string& path)
		{
			std::string result;
			std::ifstream inputStream(path);
			
			if (!inputStream.is_open())
			{
				throw ParseErrorException {};
			}

			inputStream.seekg(0, std::ios::end);
			result.reserve(static_cast<std::size_t>(inputStream.tellg()));
			inputStream.seekg(0, std::ios::beg);

			result.assign((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			return result;
		}
	}
}