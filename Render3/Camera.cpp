#include "Camera.h"
#include "glm\ext.hpp"

namespace Render3
{
	Camera::Camera(double fovy, double aspect, double near, double far, bool buildWidthFrustum)
	{
		updateProjectionMatrix(fovy, aspect, near, far, buildWidthFrustum);
	}

	Camera::~Camera()
	{
	}

	glm::mat4x4 Camera::getViewMatrix()
	{
		return glm::inverse(getMatrix());
	}

	glm::mat4x4 Camera::getProjectionMatrix()
	{
		return mProjectionMatrix;
	}

	void Camera::updateProjectionMatrix(double fovy, double aspect, double near, double far, bool buildWithFrustum)
	{
		// TODO: Refactor

		if (buildWithFrustum)
		{
			double top = near * glm::tan(glm::radians(0.5 * fovy));
			double height = 2.0 * top;
			double width = aspect * height;
			double left = -0.5 * width;
			mProjectionMatrix = glm::frustum(left, left + width, top - height, top, near, far);
			return;
		}
		// Fovy is negated so that the x and y axis which are inverted are fixed.
		mProjectionMatrix = glm::perspective(-fovy, aspect, near, far);
	}
}

