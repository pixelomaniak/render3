#include "Buffer.h"

#include <boost/assert.hpp>

#include "Common.h"

namespace Render3
{
	Buffer::Buffer(const BufferBindingTarget target,
				   const BufferUsage usage,
				   const unsigned int totalSize)

		: mTarget(static_cast<GLenum>(target)),
		  mUsage(static_cast<GLenum>(usage)),
		  mSize(totalSize),
		  mLabel()
	{
		// TODO: Consider glCreateBuffers available only in OpenGL 4.5
		glGenBuffers(1, &mBuffer);
		// TODO: Consider using std::array
		bind();
		glBufferData(mTarget, totalSize, NULL, mUsage);
		release();

		// TODO: Use UUID. Use a base class for all OpenGL resources?
		const std::string label = "Buffer";
		glObjectLabel(GL_SHADER, mBuffer, label.size(), label.c_str());
	}

	Buffer::~Buffer()
	{
		release();
		glDeleteBuffers(1, &mBuffer);
	}
	
	void Buffer::bufferSubData(const unsigned int offset,
							   const unsigned int subDataSize,
							   const void* const data)
	{
		BOOST_VERIFY_MSG(static_cast<int>(subDataSize) - static_cast<int>(offset) <= static_cast<signed>(mSize),
						"Subdata size exceeds the total size of the buffer");
		bind();
		glBufferSubData(mTarget, offset, subDataSize, data);
		release();
	}

	unsigned int Buffer::getSize() const
	{
		return mSize;
	}

	void Buffer::bind()
	{
		glBindBuffer(mTarget, mBuffer);
	}

	void Buffer::release()
	{
		glBindBuffer(mTarget, 0);
	}

	const GLuint Buffer::getBufferId() const
	{
		return mBuffer;
	}

	Vao::Vao()
	{
		glGenVertexArrays(1, &mVaoId);
	}

	void Vao::bind()
	{
		GlState::getInstance().bindVao(mVaoId);
	}

	void Vao::release()
	{
		GlState::getInstance().releaseVao(mVaoId);
	}

	Vao::~Vao()
	{
		release();
		glDeleteVertexArrays(1, &mVaoId);
	}
}
