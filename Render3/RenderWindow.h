#pragma once

#include <string>
#include <memory>
#include <functional>

#include <boost/noncopyable.hpp>

#include <SDL_video.h>

class RenderWindow : private boost::noncopyable
{
public:
	RenderWindow(int width, int height, const std::string& title);
	~RenderWindow();
	void update();
	bool isClosed();

private:
	std::unique_ptr<SDL_Window, std::function<void(SDL_Window*)>> mWindow;
	SDL_GLContext mGlContext;
	bool mIsClosed;
};

