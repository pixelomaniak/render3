#include "Transform.h"
#include <algorithm>
#include <iostream>
#include "glm\gtc\matrix_transform.hpp"

namespace Render3
{
	Transform::Transform()
		: position(glm::zero<glm::vec3>()),
		  rotation(glm::zero<glm::vec3>()),
		  scale(glm::one<glm::vec3>())
	{
	}

	Transform::Transform(const glm::vec3& position,
						 const glm::vec3& rotation,
						 const glm::vec3& scale)
		: position(position), rotation(rotation), scale(scale) {}

	Transform::~Transform() {}

	glm::mat4x4 Transform::getMatrix()
	{
		glm::mat4x4 result = getLocalMatrix();
		if (!mParent.expired() && mParent.lock() != nullptr)
			return mParent.lock()->getMatrix() * result;
		return result;
	}

	glm::mat4x4 Transform::getLocalMatrix()
	{
		glm::mat4x4 position = glm::translate(identity, this->position);
		glm::mat4x4 rotationX = glm::rotate(identity, rotation.x, right);
		glm::mat4x4 rotationY = glm::rotate(identity, rotation.y, up);
		glm::mat4x4 rotationZ = glm::rotate(identity, rotation.z, forward);
		glm::mat4x4 rotation = rotationZ * rotationY * rotationX;
		glm::mat4x4 scale = glm::scale(glm::mat4x4(1.0), this->scale);
		return position * rotation * scale;
	}

	void Transform::setParent(const std::shared_ptr<Transform>& parent)
	{
		if (parent.get() != nullptr && this != parent.get())
			std::cout << "Object can't be it's own parent" << std::endl;
		mParent = parent;
	}

	void Transform::addChild(const std::shared_ptr<Transform>& transform)
	{
		if (std::find(mChildren.begin(), mChildren.end(), transform) != mChildren.end())
			mChildren.push_back(transform);
	}

	void Transform::removeChild(const std::shared_ptr<Transform>& transform)
	{
		std::remove(mChildren.begin(), mChildren.end(), transform);
	}

	const glm::vec3 Transform::right = glm::vec3(1.0f, 0.0f, 0.0f);
	const glm::vec3 Transform::up = glm::vec3(0.0f, 1.0f, 0.0f);
	const glm::vec3 Transform::forward = glm::vec3(0.0f, 0.0f, 1.0f);
	const glm::mat4x4 Transform::identity = glm::mat4x4();
}

