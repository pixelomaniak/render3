#include "Shader.h"

#include <iostream>
#include <algorithm>
#include <memory>
#include <fstream>

#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Common.h"

namespace Render3
{
	ShaderObject::ShaderObject(const ShaderType shaderType, const std::string& source)
	{
		mShaderObject = glCreateShader(static_cast<GLenum>(shaderType));

		GLchar* shader = (GLchar*)source.c_str();
		glShaderSource(mShaderObject, 1, &shader, NULL);

		glCompileShader(mShaderObject);
		GLint status;
		glGetShaderiv(mShaderObject, GL_COMPILE_STATUS, &status);
		if (status != GL_TRUE)
		{
			// TODO: If this is going to be an SDK what then is this a correct behaviour?
			glDeleteShader(mShaderObject);
			throw ShaderCompilationFailedException {};
		}

		// TODO: Use UUID. Use a base class for all OpenGL resources?
		const std::string label = "ShaderObject";
		glObjectLabel(GL_SHADER, mShaderObject, label.size(), label.c_str());
	}

	ShaderObject::~ShaderObject()
	{
		glDeleteShader(mShaderObject);
	}

	std::string ShaderObject::getShaderLog(const ShaderObject& shaderObject)
	{
		GLint size;
		const GLuint id = shaderObject.getShaderId();
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &size);

		const std::unique_ptr<GLchar> log = std::make_unique<GLchar>(size);
		glGetShaderInfoLog(id, size, NULL, log.get());

		return std::string(log.get(), size);
	}

	GLuint ShaderObject::getShaderId() const
	{
		return mShaderObject;
	}

	ShaderProgram::ShaderProgram(const std::string& vertex,
								 const std::string& fragment,
							     const boost::optional<std::string>& tesse,
								 const boost::optional<std::string>& tessc,
								 const boost::optional<std::string>& geometry)
		: mVertex(std::make_shared<ShaderObject>(ShaderType::GlVertexShader, vertex)),
		  mFragment(std::make_shared<ShaderObject>(ShaderType::GlFragmentShader, fragment)),
		  mTesse(),
		  mTessc(),
		  mGeometry()
	{
		if (tesse.is_initialized())
		{
			mTesse.reset(std::make_shared<ShaderObject>(ShaderType::GlTesselationEvaluationShader, *tesse));
		}
		if (tessc.is_initialized())
		{
			mTessc.reset(std::make_shared<ShaderObject>(ShaderType::GlTesselationControlShader, *tessc));
		}
		if (geometry.is_initialized())
		{
			mGeometry.reset(std::make_shared<ShaderObject>(ShaderType::GlGeometryShader, *geometry));
		}
		mProgram = 0;

		initialize();
	}
	
	ShaderProgram::~ShaderProgram()
	{
		release();
		glDeleteProgram(mProgram);
	}

	std::string ShaderProgram::getProgramLog(const ShaderProgram & shaderProgram)
	{
		GLint size;
		const GLuint id = shaderProgram.getProgramId();
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &size);

		const std::unique_ptr<GLchar> log = std::make_unique<GLchar>(size);
		glGetProgramInfoLog(id, size, NULL, log.get());

		return std::string(log.get(), size);
	}

	ShaderProgramSharedPtr ShaderProgram::loadAndCreateShader(const std::string& vertexSourcePath,
															  const std::string& fragmentSourcePath,
														      const boost::optional<std::string>& tesseSourcePath,
															  const boost::optional<std::string>& tesscSourcePath,
															  const boost::optional<std::string>& geometrySourcePath)
	{
		return std::make_shared<ShaderProgram>(Utils::readAllFile(vertexSourcePath),
											   Utils::readAllFile(fragmentSourcePath),
											   (tesseSourcePath.is_initialized() ? boost::optional<std::string>(Utils::readAllFile(tesseSourcePath.get())) : boost::none),
											   (tesscSourcePath.is_initialized() ? boost::optional<std::string>(Utils::readAllFile(tesscSourcePath.get())) : boost::none),
											   (geometrySourcePath.is_initialized() ? boost::optional<std::string>(Utils::readAllFile(geometrySourcePath.get())) : boost::none));
	}

	void ShaderProgram::bind()
	{
		GlState::getInstance().bindShaderProgram(mProgram);
	}

	void ShaderProgram::release()
	{
		GlState::getInstance().releaseShaderProgram(mProgram);
	}

	void ShaderProgram::bindAttributeLocation(const GLuint index, const std::string& name)
	{
		glBindAttribLocation(mProgram, index, name.c_str());
	}

	GLint ShaderProgram::getUniformLocation(const std::string & uniformName) const
	{
		/* TODO: For sets consider using glProgramUniform. This only works for OpenGL 4.1+
		but most likely it's more performant. Moreover it doesn't require the shader
		program to be bound hence making mIsBound obsolete. */

		return glGetUniformLocation(mProgram, uniformName.c_str());
	}

	GLint ShaderProgram::getAttributeLocation(const std::string & attribName) const
	{
		return glGetAttribLocation(mProgram, attribName.c_str());
	}

	GLuint ShaderProgram::getUniformBlockIndex(const std::string & uniformBlockName) const
	{
		return glGetUniformBlockIndex(mProgram, uniformBlockName.c_str());
	}

	void ShaderProgram::setUniform(const GLint location, const GLfloat value0)
	{
		glUniform1f(location, value0);
	}

	void ShaderProgram::setUniform(const GLint location, const GLfloat value0, const GLfloat value1)
	{
		glUniform2f(location, value0, value1);
	}

	void ShaderProgram::setUniform(const GLint location, const GLfloat value0, const GLfloat value1, const GLfloat value2)
	{
		glUniform3f(location, value0, value1, value2);
	}

	void ShaderProgram::setUniform(const GLint location, const GLfloat value0, const GLfloat value1,
								   const GLfloat value2, const GLfloat value3)
	{
		glUniform4f(location, value0, value1, value2, value3);
	}

	void ShaderProgram::setUniformMatrix4fv(const GLint location, const GLsizei count,
										    const GLboolean transpose, const GLfloat* const value)
	{
		glUniformMatrix4fv(location, count, transpose, value);
	}

	void ShaderProgram::setUniform(const GLint location, const glm::vec2& value)
	{
		setUniform(location, value.x, value.y);
	}

	void ShaderProgram::setUniform(const GLint location, const glm::vec3& value)
	{
		setUniform(location, value.x, value.y, value.z);
	}

	void ShaderProgram::setUniform(const GLint location, const glm::vec4& value)
	{
		setUniform(location, value.x, value.y, value.z, value.w);
	}

	void ShaderProgram::setUniform(const GLint location, const glm::mat4& value, const GLboolean transpose)
	{
		setUniformMatrix4fv(location, 1, transpose, &value[0][0]);
	}

	GLuint ShaderProgram::getProgramId() const
	{
		return mProgram;
	}

	void ShaderProgram::initialize()
	{
		mProgram = glCreateProgram();

		glAttachShader(mProgram, mVertex->getShaderId());

		if (mTesse.is_initialized())
		{
			glAttachShader(mProgram, mTesse.get()->getShaderId());
		}
		if (mTessc.is_initialized())
		{
			glAttachShader(mProgram, mTessc.get()->getShaderId());
		}
		if (mGeometry.is_initialized())
		{
			glAttachShader(mProgram, mGeometry.get()->getShaderId());
		}
		glAttachShader(mProgram, mFragment->getShaderId());

		glLinkProgram(mProgram);
		GLint status;
		glGetProgramiv(mProgram, GL_LINK_STATUS, &status);
		if (status != GL_TRUE)
		{
			glDeleteProgram(mProgram);
			throw ShaderLinkageFailedException {};
		}

		// TODO: Use UUID. Use a base class for all OpenGL resources?
		const std::string label = "ShaderProgramObject";
		//glObjectLabel(GL_SHADER, mProgram, label.size(), label.c_str());
	}
}
