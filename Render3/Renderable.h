#pragma once

#include <memory>

#include <boost/noncopyable.hpp>

#include "Shader.h"
#include "Buffer.h"

namespace Render3
{
	enum class IndexType
	{
		GlUnsignedByte = GL_UNSIGNED_BYTE,
		GlUnsignedShort = GL_UNSIGNED_SHORT,
		GlUnsignedInt = GL_UNSIGNED_INT
	};

	enum class RenderMode
	{
		// TODO: Add everything else based on specifications @http://docs.gl/gl4/glDrawElements
		GlPoints = GL_POINTS,
		GlTriangles = GL_TRIANGLES
	};

	class IRenderable
	{
	public:
		virtual void render(const ShaderProgramSharedPtr& shaderProgram) = 0;
		virtual void prepare(const ShaderProgramSharedPtr& shaderProgram) = 0;

		virtual ~IRenderable() = default;
	};
}
