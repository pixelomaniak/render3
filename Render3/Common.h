#pragma once

#include <memory>
#include <string>
#include <exception>

#include <boost/noncopyable.hpp>
#include <boost/exception/exception.hpp>
#include <boost/assert.hpp>

#include "GL/glew.h"

#define CHECK_GL_STATE() BOOST_ASSERT(Render3::GlError::checkGlState())

namespace Render3
{
	enum class GlErrorType
	{
		GlNoError = GL_NO_ERROR,
		GlInvalidEnum = GL_INVALID_ENUM,
		GlInvalidValue = GL_INVALID_VALUE,
		GlInvalidOperation = GL_INVALID_OPERATION,
		GlInvalidFramebufferOperation = GL_INVALID_FRAMEBUFFER_OPERATION,
		GlOutOfMemory = GL_OUT_OF_MEMORY,
		GlStackUnderflow = GL_STACK_UNDERFLOW,
		GlStackOverfloat = GL_STACK_OVERFLOW
	};

	class GlError
	{
	public:
		static GlErrorType getGlError();
		static bool checkGlState();
	};

	class GlState : private boost::noncopyable 
	{
	public:
		GlState() = default;
		~GlState() = default;

		static GlState& getInstance();

		// TODO: Use abstractions instead i.e ShaderProgram, Buffer, etc..
		const bool bindShaderProgram(const GLuint shaderProgram);
		const bool releaseShaderProgram(const GLuint shaderProgram);
		const bool bindVao(const GLuint vao);
		const bool releaseVao(const GLuint vao);


	private:
		GLuint mCurrentlyBoundShaderProgram;
		GLuint mCurrentlyBoundBuffer;
		GLuint mCurrentlyBoundVao;
	};

	namespace Utils
	{
		struct ParseErrorException : public boost::exception, std::exception
		{
			// TODO: what?
		};

		std::string readAllFile(const std::string& path);
	}
}