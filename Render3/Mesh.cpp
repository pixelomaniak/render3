#include "Mesh.h"

#include "Common.h"

namespace Render3
{
	Mesh::Mesh(const GeometrySharedPtr geometry)
		: mGeometry(geometry),
		  mPositionBuffer(),
		  mTexCoordBuffer(),
		  mNormalBuffer(),
		  mIndexBuffer(),
		  mVao(std::make_shared<Vao>())
	{
		std::vector<glm::vec3> positions = mGeometry->getPositions();
		std::vector<glm::vec2> texCoords = mGeometry->getUvCoords();
		std::vector<glm::vec3> normals = mGeometry->getNormals();
		const std::vector<unsigned int>& indices = mGeometry->getIndices();

		mPositionBuffer = std::make_shared<Buffer>(BufferBindingTarget::GlArrayBuffer,
											       BufferUsage::GlStaticDraw,
												   positions.size() * sizeof(glm::vec3));
		mTexCoordBuffer = std::make_shared<Buffer>(BufferBindingTarget::GlArrayBuffer,
												   BufferUsage::GlStaticDraw,
												   texCoords.size() * sizeof(glm::vec2));
		mNormalBuffer = std::make_shared<Buffer>(BufferBindingTarget::GlArrayBuffer,
												 BufferUsage::GlStaticDraw,
												 normals.size() * sizeof(glm::vec3));
		mIndexBuffer = std::make_shared<Buffer>(BufferBindingTarget::GlElementArrayBuffer,
												BufferUsage::GlStaticDraw,
												indices.size() * sizeof(GLuint));

		mPositionBuffer->bufferSubData(0, positions.size() * sizeof(glm::vec3), positions.data());
		mTexCoordBuffer->bufferSubData(0, texCoords.size() * sizeof(glm::vec2), texCoords.data());
		mNormalBuffer->bufferSubData(0, normals.size() * sizeof(glm::vec3), normals.data());
		mIndexBuffer->bufferSubData(0, indices.size() * sizeof(GLuint), indices.data());
	}

	void Mesh::render(const ShaderProgramSharedPtr& shaderProgram)
	{
		mVao->bind();
		shaderProgram->bind();
		// TODO: Avoid direct call to OpenGL in here.
		
		glDrawElements(GL_TRIANGLES, mGeometry->getIndices().size(), GL_UNSIGNED_INT, NULL);
	}
	
	void Mesh::prepare(const ShaderProgramSharedPtr& shaderProgram)
	{
		mVao->bind();

		mIndexBuffer->bind();

		// TODO: Abstract this with an "AttributeInfo" class.
		GLuint location = shaderProgram->getAttributeLocation("location");
		GLuint texCoord = shaderProgram->getAttributeLocation("uv");
		GLuint normal = shaderProgram->getAttributeLocation("normal");
		
		// TODO: Avoid direct call to OpenGL in here.
		if (location != -1)
		{
			mPositionBuffer->bind();
			glVertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(location);
		}
		if (texCoord != -1)
		{
			mTexCoordBuffer->bind();
			glVertexAttribPointer(texCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(texCoord);
		}
		if (normal != -1)
		{
			mNormalBuffer->bind();
			glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(normal);
		}

		mVao->release();
	}
}
