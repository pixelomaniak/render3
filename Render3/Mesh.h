#pragma once

#include <memory>
#include <vector>

#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>

#include "glm/glm.hpp"

#include "Geometry.h"
#include "Renderable.h"
#include "Transform.h"

namespace Render3
{
	class Mesh : public IRenderable,
				 public Transform
	{
	public:
		Mesh(const GeometrySharedPtr geometry);

		// From IRenderable
		virtual void render(const ShaderProgramSharedPtr& shaderProgram) override;
		virtual void prepare(const ShaderProgramSharedPtr& shaderProgram) override;
		
	private:
		GeometrySharedPtr mGeometry;
		BufferSharedPtr mPositionBuffer;
		BufferSharedPtr mTexCoordBuffer;
		BufferSharedPtr mNormalBuffer;
		BufferSharedPtr mIndexBuffer;
		VaoSharedPtr mVao;
	};
}
