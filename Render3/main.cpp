#include <iostream>
#include <chrono>

#include <GL/glew.h>
#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include "Common.h"
#include "RenderWindow.h"
#include "Shader.h"
#include "Camera.h"
#include "Mesh.h"
#include "Geometry.h"
#include "Buffer.h"

using Render3::ShaderProgram;
using Render3::GeometryBuilder;
using Render3::Mesh;

int main(int args, char** argv)
{
	RenderWindow renderWindow(512u, 512u, "Rendering Engine");

	auto shader = ShaderProgram::loadAndCreateShader("./resources/shaders/colorful.vertex",
												     "./resources/shaders/colorful.fragment");
	auto mesh = std::make_shared<Mesh>(GeometryBuilder::buildQuad());

	glFrontFace(GL_CW);
	glCullFace(GL_BACK);

	glDisable(GL_CULL_FACE);
	mesh->prepare(shader);
	while (!renderWindow.isClosed())
	{
		glClear(GL_COLOR_BUFFER_BIT);
		mesh->render(shader);
		renderWindow.update();
	}
	
	return 0;
} 