#pragma once

#include "Transform.h"

#include <boost/noncopyable.hpp>

namespace Render3
{
	class Camera : public Transform,
				   private boost::noncopyable
	{
	public:
		Camera(double fovy, double aspect, double near, double far, bool buildWidthFrustum = false);
		virtual ~Camera();

		glm::mat4x4 getViewMatrix();
		glm::mat4x4 getProjectionMatrix();
		void updateProjectionMatrix(double fovy, double aspect, double near, double far, bool buildWithFrustum = false);

	private:
		glm::mat4x4 mProjectionMatrix;
	};
}

