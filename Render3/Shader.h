#pragma once

#include <string>
#include <utility>
#include <vector>
#include <memory>
#include <exception>

#include <boost/core/noncopyable.hpp>
#include <boost/optional.hpp>
#include <boost/exception/exception.hpp>

#include <glm/common.hpp>
#include "GL/glew.h"

namespace Render3
{
	struct ShaderCompilationFailedException : public boost::exception,
											  public std::exception
	{
	};

	struct ShaderLinkageFailedException : public boost::exception,
										  public std::exception
	{
	};
}

namespace Render3
{
	enum class ShaderType
	{
		GlVertexShader = GL_VERTEX_SHADER,
		GlTesselationControlShader = GL_TESS_CONTROL_SHADER,
		GlTesselationEvaluationShader = GL_TESS_EVALUATION_SHADER,
		GlGeometryShader = GL_GEOMETRY_SHADER,
		GlFragmentShader = GL_FRAGMENT_SHADER
	};

	class ShaderObject : private boost::noncopyable
	{
	public:
		friend class ShaderProgram;

	public:
		ShaderObject(const ShaderType shaderType, const std::string& source);
		virtual ~ShaderObject();

		static std::string getShaderLog(const ShaderObject& shaderObject);

	private:
		GLuint getShaderId() const;

	private:
		GLuint mShaderObject;
		std::string mLabel;
	};

	class ShaderProgram : private boost::noncopyable
	{
	public:

		ShaderProgram(const std::string& vertex,
					  const std::string& fragment,
					  const boost::optional<std::string>& tesse = boost::none,
					  const boost::optional<std::string>& tessc = boost::none,
					  const boost::optional<std::string>& geometry = boost::none);

		virtual ~ShaderProgram();

		static std::string getProgramLog(const ShaderProgram& shaderProgram);
		static std::shared_ptr<ShaderProgram> loadAndCreateShader(const std::string& vertexSourcePath,
																  const std::string& fragmentSourcePath,
																  const boost::optional<std::string>& tesseSourcePath = boost::none,
																  const boost::optional<std::string>& tesscSourcePath = boost::none,
																  const boost::optional<std::string>& geometrySourcePath = boost::none);

		void bind();
		void release();
		void bindAttributeLocation(const GLuint index, const std::string& name);

		GLint getUniformLocation(const std::string& uniformName) const;
		GLint getAttributeLocation(const std::string& attribName) const;
		GLuint getUniformBlockIndex(const std::string& uniformBlockName) const;

		// TODO: Add rest of the functions from specification @http://docs.gl/gl4/glUniform
		void setUniform(const GLint location, const GLfloat value0);
		void setUniform(const GLint location, const GLfloat value0, const GLfloat value1);
		void setUniform(const GLint location, const GLfloat value0, const GLfloat value1, const GLfloat value2);
		void setUniform(const GLint location, const GLfloat value0, const GLfloat value1,
						  const GLfloat value2, const GLfloat value3);
		void setUniformMatrix4fv(const GLint location, const GLsizei count,
								 const GLboolean transpose, const GLfloat* const value);

		void setUniform(const GLint location, const glm::vec2& value);
		void setUniform(const GLint location, const glm::vec3& value);
		void setUniform(const GLint location, const glm::vec4& value);
		void setUniform(const GLint location, const glm::mat4& value, const GLboolean transpose = GL_FALSE);

	public:
		GLuint getProgramId() const;
		void initialize();

	private:
		std::shared_ptr<ShaderObject> mVertex;
		std::shared_ptr<ShaderObject> mFragment;
		boost::optional<std::shared_ptr<ShaderObject>> mTesse;
		boost::optional<std::shared_ptr<ShaderObject>> mTessc;
		boost::optional<std::shared_ptr<ShaderObject>> mGeometry;

		GLuint mProgram;

		std::string mLabel;
	};

	using ShaderObjectSharedPtr = std::shared_ptr<ShaderObject>;
	using ShaderProgramSharedPtr = std::shared_ptr<ShaderProgram>;
}
